<?php
    session_start();
    if(isset($_SESSION['email'])){
?>
    <!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" 
    integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="estilos.css" />
</head>
<body style="background-image: url('imagenes/pergamino2.jpg'); background-attachment:fixed;" >

      <h1>Historia Antigua</h1>
      <div class="fLogin">
          <h5>Bienvenid@ <?php echo $_SESSION['email'] ?></h5>
          <a href="cerrar.php"><i class="material-icons" style="color: white">close</i></a>
      </div>
      <div class="pill-nav">
      <a class="active" href="principal.php">PORTAL</a>
      <a href="articulos.php" style="color: white;">ARTICULOS</a>
      <a href="registro.php" style="color: white;">REGISTRO</a>
      <a href="contacto.php" style="color: white;">CONTACTO</a>
		<br/>
    </div>
    <div>
        <h1>Ya estás registrado  <?php echo $_SESSION['email']?></h1>
    </div>
</body>
</html>
<?php
  }else{
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" 
    integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="estilos.css" />
</head>
<body style="background-image: url('imagenes/pergamino2.jpg'); background-attachment:fixed">

      <h1>Historia Antigua</h1>
      <div class="pill-nav">
      <a class="active" href="principal.php">PORTAL</a>
      <a href="articulos.php" style="color: white;">ARTICULOS</a>
      <a href="registro.php" style="color: white;">REGISTRO</a>
      <a href="contacto.php" style="color: white;">CONTACTO</a>
		<br/>
    </div>
    <form method="POST" action="registrar.php" id="idRegistro" name="registro">
    <div class="form-row">
        <div class="form-group col-md-6">
        <input type="text" class="form-control" placeholder="Nombre" name="txtNombre" required /></br>
      </div>
      <div class="form-group col-md-6">
        <input type="password" class="form-control" placeholder="Contraseña" name="txtPassword" required/></br>
      </div>
      <div class="form-group col-md-6">
        <input type="text" class="form-control" placeholder="Apellidos" name="txtApellidos"   required/></br>
      </div>
      <div class="form-group col-md-6">
        <input type="text" class="form-control" placeholder="Número de teléfono" name="txtTelefono" maxlength="9" required/></br>
      </div>
      <div class="form-group col-md-6">
        <input type="text" class="form-control" placeholder="Dirección" name="txtDire" required/></br>
      </div>
      <div class="form-group col-md-6">
        <input type="date" class="form-control"  name="txtEdad" /></br>
      </div>
      <div class="form-group col-md-6">
        <input type="email"  class="form-control" name="txtEmail" placeholder="Email" id="idEmail" required/></br>
      </div>
      <div class="form-group col-md-6">
        <input type="text" class="form-control"  name="txtPoblacion" placeholder="Población" required/></br>
      </div>
    </div>
        <input type="submit" class="boton" value="Registrarse" name="btAlta" id="idAlta" />
  </form>

</body>
</html>
<?php
  }

?>