<?php

include "conexion.php";
$codigo = $_GET['idArticulo'];

$cmdConsulta="SELECT titulo,texto, foto FROM articulos WHERE idArticulo = $codigo";

$datos=mysqli_query($conexion,$cmdConsulta);

$fila = mysqli_fetch_assoc($datos);

?>
  <!DOCTYPE html>
        <html lang="en">
        <head>
            <meta charset="UTF-8">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <meta http-equiv="X-UA-Compatible" content="ie=edge">
            <title>Document</title>
            <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" 
            integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
            <link href="https://fonts.googleapis.com/css?family=Acme|Ma+Shan+Zheng&display=swap" rel="stylesheet">
            <link rel="stylesheet" type="text/css" href="estilos.css" />
            <script src="ajax.js"></script>
            <style>
                h1{
                    font-family: 'Acme', sans-serif;
                }
                h2{
                   margin: auto;
                   text-align: center;
                   color: darkred;
                   font-family: 'Acme', sans-serif;
                   margin-bottom: 2%;
                   font-size: 70px;
                }
                img{
                    display:block;
                    margin:auto;
                    margin-bottom: 1%;
                }
                p{
                    display:block;
                    margin:auto;
                    color: black;
                    text-align: center;
                    font-family:'Acme', sans-serif;
                    font-size: 40px;
                  
                    height: 80%;
                    width: 50%;
                    border: 10px thin darkred;
                    
                }

            </style>
        </head>
        <body style="background-image: url('imagenes/pergamino2.jpg'); background-attachment:fixed">
            <h1>Historia Antigua</h1>
             <?php
                    header('Content-Type: text/html; charset=utf-8');
					echo '<div>';
					echo "<h2>$fila[titulo]</h2>";	
					$foto="fotosAlmacenadas/" . $fila['foto'];
					echo '<img src="'.$foto.'" />';
					echo "<p>$fila[texto]</p>";
					echo '<hr />';		 
					echo '</div>';

			?>	
        </body>
        </html>