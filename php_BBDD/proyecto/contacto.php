<?php
    session_start();
    if(isset($_SESSION['email'])){
?>
        <!DOCTYPE html>
        <html lang="en">
        <head>
            <meta charset="UTF-8">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <meta http-equiv="X-UA-Compatible" content="ie=edge">
            <title>Document</title>
            <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" 
            integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
            <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
            <link rel="stylesheet" type="text/css" href="estilos.css" />
        </head>
        <body style="background-image: url('imagenes/pergamino2.jpg'); background-attachment:fixed;" >
            <h1>Historia Antigua</h1>
            <div class="fLogin">
                <h5>Bienvenid@ <?php echo $_SESSION['email'] ?></h5>
                <a href="cerrar.php"><i class="material-icons" style="color: white">close</i></a>
            </div>
            <div class="pill-nav">
                <a class="active" href="principal.php">PORTAL</a>
                <a href="articulos.php" style="color: white;">ARTICULOS</a>
                <a href="registro.php" style="color: white;">REGISTRO</a>
                <a href="contacto.php" style="color: white;">CONTACTO</a>
                <br/>
            </div>
            <div class="contacto" style="text-align: center; font-family: fantasy; font-size: 40px; 
                margin-top: 5%; align-content: center; margin-left: 20%; margin-right: 20%;">
                <p>Esta página no tiene ningún espacio real, para cualquier consulta debe dirigirse
                    al siguiente correo: nksars92@gmail.com.</p>
            </div>
        </body>
        </html>

<?php
    }else{?>
        <!DOCTYPE html>
        <html lang="en">
        <head>
            <meta charset="UTF-8">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <meta http-equiv="X-UA-Compatible" content="ie=edge">
            <title>Document</title>
            <link rel="stylesheet" type="text/css" href="estilos.css" />
        </head>
        <body style="background-image: url('imagenes/pergamino2.jpg'); background-attachment:fixed">
            <h1>Historia Antigua</h1>
            <div class="pill-nav">
                <a class="active" href="principal.php">PORTAL</a>
                <a href="articulos.php" style="color: white;">ARTICULOS</a>
                <a href="registro.php" style="color: white;">REGISTRO</a>
                <a href="contacto.php" style="color: white;">CONTACTO</a>
                <br/>
            </div>
            <div class="contacto" style="text-align: center; font-family: fantasy; font-size: 40px; 
                margin-top: 5%; align-content: center; margin-left: 20%; margin-right: 20%;">
                <p style="color: darkred">Esta página no tiene ningún espacio real, para cualquier consulta debe dirigirse
                    al siguiente correo: nksars92@gmail.com.</p>
            </div>
        </body>
        </html>
<?php    
    }
?>