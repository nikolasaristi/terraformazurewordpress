<?php
	#Esta pagina php se encarga de hacer la insert de los usuarios en la base de datos historia en la tabla usuarios
	if(isset($_POST['btAlta']))  
	{	/* ******* si no es la primera solicitud ***********/
	    // Preparación de la conexion
		$conexion = mysqli_connect('localhost','root','','historia');  
		if (! $conexion) {  
	  		exit('Error de conexion.');  
		}  
		// Selección de la base de datos.  
		$bd = mysqli_select_db($conexion,'historia');  
		if (! $bd) {  
	    	exit('No se pudo seleccionar la base de datos.');  
		}  
	
	    // preparamos la consulta INSERT.  
		$sql = 'INSERT INTO usuarios (nombre,apellidos,numTelefono,direccion,edad, password1, email, poblacion) VALUES (?,?,?,?,?,?,?,?)';
		$cmdAlta = mysqli_prepare($conexion,$sql);  
		
		// Asociamos los parámetros si la consulta tiene parámetros.  
		$acoplador = mysqli_stmt_bind_param($cmdAlta,'ssssssss',$nombre,$apellidos,$numTelefono,$direccion,$edad,$password1,$email,$poblacion);  
		
		// Ejecutamos la consulta asignando previamente los valores
		// a las variables acopladas
		$nombre=$_POST['txtNombre'];
		$apellidos=$_POST['txtApellidos'];
		$numTelefono=$_POST['txtTelefono'];
		$direccion=$_POST['txtDire'];
		$edad=$_POST['txtEdad'];
		$password1=$_POST['txtPassword'];
		$email=$_POST['txtEmail'];
		$poblacion=$_POST['txtPoblacion'];
	
		mysqli_stmt_execute($cmdAlta);
		
		// Damos la bienvenida al nuevo usuario
		echo "<h1>$email se ha registrado satisfactoriamente.</h1><br />"; 	 		
   }
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Document</title>
</head>
<body>
	<a href="principal.php">Página de inicio</a>
</body>
</html>
