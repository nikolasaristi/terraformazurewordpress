provider "azurerm" {
    version = 1.38
    }

# Create virtual network
resource "azurerm_virtual_network" "vnet_wordpress_v1" {
    name                = "vnet_wordpress_v1"
    address_space       = ["10.0.0.0/16"]
    location            = "eastus"
    resource_group_name = "1-9f489b05-playground-sandbox"

    tags = {
        environment = "Terraform Networking"
    }
}
#CREAR NSG
resource "azurerm_network_security_group" "wpnsg" {
  name                = "wordpressNSG"
  location            = "East US"
  resource_group_name = "1-9f489b05-playground-sandbox"
}

resource "azurerm_network_security_rule" "example1" {
  name                        = "Web80"
  priority                    = 1001
  direction                   = "Inbound"
  access                      = "Allow"
  protocol                    = "Tcp"
  source_port_range           = "80"
  destination_port_range      = "80"
  source_address_prefix       = "*"
  destination_address_prefix  = "*"
  resource_group_name         = "1-9f489b05-playground-sandbox"
  network_security_group_name = azurerm_network_security_group.wpnsg.name
}

resource "azurerm_network_security_rule" "example2" {
  name                        = "Web8080"
  priority                    = 1000
  direction                   = "Inbound"
  access                      = "Deny"
  protocol                    = "Tcp"
  source_port_range           = "8080"
  destination_port_range      = "8080"
  source_address_prefix       = "*"
  destination_address_prefix  = "*"
  resource_group_name         = "1-9f489b05-playground-sandbox"
  network_security_group_name = azurerm_network_security_group.wpnsg.name
}