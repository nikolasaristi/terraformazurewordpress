<?php
	function subirFichero($ficheroOrigen, $directorioDestino, $ficheroDestino)
	{
	    $temporal = $_FILES[$ficheroOrigen]['tmp_name'];
	    //echo $temporal . '<br />';
	    $destino= $directorioDestino . $ficheroDestino;
	    //echo $destino;
	    //si hemos enviado un directorio que existe realmente y hemos subido el archivo    
	    if (is_dir($directorioDestino) && is_uploaded_file($temporal))
	    {
	        //$img_file = $_FILES[$ficheroOrigen]['name'];
	        $tipoImagen = $_FILES[$ficheroOrigen]['type'];
	        
	        //echo $tipoImagen;
	        // Si se trata de una imagen   
	        if (((strpos($tipoImagen, "gif") || strpos($tipoImagen, "jpeg") ||
	 			strpos($tipoImagen, "jpg")) || strpos($tipoImagen, "png")))
	        {
	            //¿Tenemos permisos para subir la imágen?	            
	            if (move_uploaded_file($temporal, $directorioDestino . $ficheroDestino))
	            {
	                return true;
	            }
	        }
	    }
	    //Si llegamos hasta aquí es que algo ha fallado
	    echo "mal";
	    return false;
	}
?>